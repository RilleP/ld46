extends RigidBody2D


var is_driving = false;
var just_started_driving = false;

var driver = null;

var engine_sound_fade_time = 1.0;
var engine_sound_fade_timer = 0.0;
var engine_sound_fade_direction = 0;

func set_indicated(indicated):
	if indicated:
		$Sprite.material = load("fuel/shine_material.tres");
	else:
		$Sprite.material = null;

func start_driving(guy):
	is_driving = true;
	guy.driving = self;
	guy.get_parent().remove_child(guy);
	driver = guy;
	just_started_driving = true;
	$DriverSprite.visible = true;
	$AudioStreamPlayer2D.play();
	#engine_sound_fade_timer = 0;
	engine_sound_fade_direction = 1;
	return true;
	
func stop_driving(guy):
	if !is_driving: 
		return;
	is_driving = false;
	guy.driving = null;
	guy.position = position + Vector2(0, -200);
	get_parent().add_child(guy);
	driver = null;
	$DriverSprite.visible = false;
	
	engine_sound_fade_direction = -1;
	return false;
	
func _process(delta):
	if get_node("/root/first").game_over: return;
	
	if engine_sound_fade_direction != 0:
		engine_sound_fade_timer += delta*engine_sound_fade_direction;
		if(engine_sound_fade_direction < 0 && engine_sound_fade_timer <= 0):
			$AudioStreamPlayer2D.stop();
			engine_sound_fade_direction = 0;
			engine_sound_fade_timer = 0;
		elif(engine_sound_fade_direction > 0 && engine_sound_fade_timer >= 1):
			engine_sound_fade_direction = 0;
			engine_sound_fade_timer = 1;
		
		$AudioStreamPlayer2D.volume_db = lerp(-80, 0, min(1, engine_sound_fade_timer / engine_sound_fade_time));
	
	if is_driving:
		if !just_started_driving && Input.is_action_just_pressed("action"):
			stop_driving(driver);
			
		get_parent().get_node("Camera2D").position.x = position.x;
		
	just_started_driving = false;
	
func _physics_process(delta):
	if get_node("/root/first").game_over: return;
	if !is_driving: return;
	var grounded = get_colliding_bodies().size() != 0;
	
	$AudioStreamPlayer2D.pitch_scale = 1.0 + min(0.3, abs(linear_velocity.x) / 1000.0);
	var move_direction = Vector2.ZERO;
	if Input.is_action_pressed("move_right"):
		move_direction.x += 1;
	if Input.is_action_pressed("move_left"):
		move_direction.x -= 1;
		
	if move_direction.length_squared() > 0:
		var move_speed = 30000;
		if !grounded:
			move_speed *= 0.5;
		apply_central_impulse(move_direction * delta * move_speed);
