extends RigidBody2D

export var start_as_burning = false;
var burning = false;
var fire = null;
var smoke = null;

const start_burning_heat = 1;
const start_smoking_heat = 0.5;
var heat = 0;

var moist = 0;
const max_moist = 5.0;

var holder = null;
var hold_offset;

var was_thrown_timer = 0;

var gained_heat_timer = 0.0;

onready var initial_mass = mass;
onready var initial_collision_layer = collision_layer;

export var starting_fuel = 120;
onready var remaining_fuel = starting_fuel;

signal on_release_from_holder;

const wet_sounds = [
	preload("res://fuel/wet_fuel1.ogg"),
];

const min_wet_sound_cooldown = 0.0;
const max_wet_sound_cooldown = 1.0;
onready var wet_sound_cooldown = 0;


# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.material = load("fuel/log_material.tres").duplicate();
	update_moist_heat_color();
	if start_as_burning:
		add_heat(3);
	#else:
	#	add_moist(5);
		
func set_indicated(indicated):
	$Sprite.material.set_shader_param("indicated", indicated);
	#if(indicated):
	#	$Sprite.material = load("fuel/shine_material.tres");
	#else:
	#	$Sprite.material = null;
		
func pick_up(by):
	collision_layer = 1 << 2;
	collision_mask &= ~(1 << 1);
	holder = by;
	hold_offset = position - by.position;
	hold_offset.y = -30;
	hold_offset.x = 20 * by.scale.x;
	mass = initial_mass * 0.2;
	#mode = RigidBody2D.MODE_KINEMATIC;

func drop(by):
	if holder == by:
		holder = null;
		was_thrown_timer = 0.5;		
		mode = RigidBody2D.MODE_RIGID;
		mass = initial_mass;
		linear_velocity = by.linear_velocity*1.2;
		
func _physics_process(delta):
	if holder != null:
		var dest_position = holder.position + hold_offset;
		
		var offset = position - dest_position;
		self.linear_velocity = -offset/delta/20;

func remove():
	emit_signal("on_release_from_holder");
	queue_free();

func _process(delta):
	wet_sound_cooldown -= delta;
	if holder != null:
		hold_offset.x = 20 * holder.look_direction;
	if was_thrown_timer > 0:
		was_thrown_timer -= delta;
		if(was_thrown_timer <= 0):
			collision_layer = initial_collision_layer;
			collision_mask |= (1 << 1);
	if burning:
		var removed_fuel = delta;
		get_node("/root/first").add_score(min(remaining_fuel, removed_fuel) * heat);
		remaining_fuel -= removed_fuel;
		
		var lightness = remaining_fuel / starting_fuel;
		$Sprite.self_modulate = Color(lightness, lightness, lightness, 1);
		$Sprite.scale = Vector2.ONE * lerp(0.8, 1, lightness);
		update_moist_heat_color();
		if(remaining_fuel <= 0):
			remove();
			return;
	elif heat > 0:
		if gained_heat_timer > 0:
			gained_heat_timer -= delta;
		else:
			remove_heat(delta*0.5);
			
	if fire != null:
		fire.heat = heat;
	if moist > 0:
		moist -= delta * 0.05;
		if moist < 0:
			moist = 0;
		update_moist_heat_color();

func update_moist_heat_color2():
	if heat > 0:
		var red = heat / 3.0;
		$Sprite.modulate = Color(1, 1 - red, 1 - red, 1);
	elif moist > 0:
		var blue = moist;
		$Sprite.modulate = Color(1 - blue, 1 - blue/2.0, 1, 1);
	else:
		$Sprite.modulate = Color.white;
		
func update_moist_heat_color():
	var fuel_mul = remaining_fuel / starting_fuel
	$Sprite.material.set_shader_param("heat", heat*fuel_mul);
	$Sprite.material.set_shader_param("moist", (moist / max_moist) *fuel_mul);

func remove_heat(amount):	
	heat -= amount;
	if heat < 0:
		heat = 0;		
	if heat < start_smoking_heat:
		stop_smoking();
	elif heat < start_burning_heat:
		stop_burning();
		
	update_moist_heat_color();

func maybe_play_wet_sound(power):
	if $WetPlayer.playing:
		return;
		
	if wet_sound_cooldown <= 0:
		$WetPlayer.pitch_scale = rand_range(0.8, 1.0);
		$WetPlayer.volume_db = min(-1, -10 + power*2);
		$WetPlayer.play();
		wet_sound_cooldown = rand_range(min_wet_sound_cooldown, max_wet_sound_cooldown);

func add_heat(amount):
	gained_heat_timer = 1.0;
	amount /= (heat*2+1);
	var removed_moist = 0.0;
	if moist > 0:
		if moist > 2.0:
			maybe_play_wet_sound(moist);
		moist -= amount;
		if moist < 0:
			removed_moist = amount + moist;
			amount = -moist;
			moist = 0;
		else:
			update_moist_heat_color();
			return amount;
			
	heat += amount;
	if(heat >= start_smoking_heat):
		start_smoking();
	if(heat >= start_burning_heat):
		emit();
		
	update_moist_heat_color();
	return removed_moist;
		
func add_moist(amount):
	if heat > 0:
		heat -= amount;
		
		if heat < start_smoking_heat:
			stop_smoking();
		elif heat < start_burning_heat:
			stop_burning();
		
		if heat < 0:
			amount = -heat;
			heat = 0;
		else:
			update_moist_heat_color();
			return;
	
	moist = min(moist + amount, max_moist);
	update_moist_heat_color();
	

func stop_smoking():
	if fire != null:
		fire.queue_free();
		fire = null;
	burning = false;

func start_smoking():
	if fire == null:
		add_fire();
	
	var fire_amount = 0;
	if burning:
		fire_amount = 1;
	fire.set_fire_and_smoke_amount(fire_amount, 1);

func add_fire():
	fire = load("fire/fire.tscn").instance();
	add_child(fire);
	fire.set_shape_with_collision_shape($CollisionShape2D.shape);
	fire.set_fire_and_smoke_amount(0, 1);

func stop_burning():
	if !burning:
		return;
	burning = false;
	if fire != null:
		fire.set_fire_and_smoke_amount(0, 1);

func emit():
	if burning: 
		return;
	burning = true;
	if fire == null:
		add_fire();
	fire.set_fire_and_smoke_amount(1, 1);


func _on_log_body_entered(body):
	if linear_velocity.length_squared() > 200*200:
		var mindb = -30;
		var maxdb = 0;
		$AudioStreamPlayer2D.volume_db = lerp(mindb, maxdb, min(linear_velocity.length()/1000, 1));
		$AudioStreamPlayer2D.pitch_scale = rand_range(0.8, 1.1);
		$AudioStreamPlayer2D.play();


