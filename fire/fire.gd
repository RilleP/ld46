extends Node2D

onready var fire_amount_per_area = $fire_particles.amount;
onready var smoke_amount_per_area = $smoke_particles.amount;

const burning_sounds = [
	preload("res://fire/fire1.ogg"),
	preload("res://fire/fire2.ogg"),
	preload("res://fire/fire3.ogg"),
];

const min_sound_cooldown = 0.0;
const max_sound_cooldown = 1.0;
onready var sound_cooldown = rand_range(min_sound_cooldown, max_sound_cooldown);

var heat = 0;


func _ready():
	update_particle_amount();

const EMISSION_SHAPE_POINT = 0
const EMISSION_SHAPE_SPHERE = 1
const EMISSION_SHAPE_BOX = 2;
const EMISSION_SHAPE_POINTS = 3;
const EMISSION_SHAPE_DIRECTED_POINTS = 4;
func set_shape_with_collision_shape(shape):
	var shape_type = EMISSION_SHAPE_POINT;
	var spread_shape = null;
	if shape is CircleShape2D:
		shape_type = EMISSION_SHAPE_SPHERE;
		$fire_particles.emission_sphere_radius = shape.radius;
		$smoke_particles.emission_sphere_radius = shape.radius;
		spread_shape = CircleShape2D.new();
		spread_shape.radius = shape.radius + 10;
		
	elif shape is RectangleShape2D:
		shape_type = EMISSION_SHAPE_BOX;
		spread_shape = RectangleShape2D.new()
		spread_shape.extents = shape.extents + Vector2(40, 40);
		$fire_particles.emission_rect_extents = shape.extents;
		$smoke_particles.emission_rect_extents = shape.extents;
		
	$spread_area/CollisionShape2D.shape = spread_shape;
	$fire_particles.emission_shape = shape_type;
	$smoke_particles.emission_shape = shape_type;
	
func call_method_on_children(node, method_name, args):
	if node.has_method(method_name):
		return node.call(method_name, args);
	for child in node.get_children():
		var result = call_method_on_children(child, method_name, args);
		if result != null:
			return result;
	return null;

func _process(delta):
	for body in $spread_area.get_overlapping_bodies():
		if body != get_parent(): 
			var transfered_moist = call_method_on_children(body, "add_heat", (heat * delta * 0.1));
			if transfered_moist != null:
				call_method_on_children(get_parent(), "add_moist", transfered_moist*0.5);
	if changed_amount:
		changed_amount = false;
		update_particle_amount();
		
	if !$AudioStreamPlayer2D.playing and fire_amount > 0:
		if sound_cooldown > 0:
			sound_cooldown -= delta;
			if sound_cooldown <= 0:
				$AudioStreamPlayer2D.pitch_scale = rand_range(0.8, 1.1);
				$AudioStreamPlayer2D.stream = burning_sounds[randi() % burning_sounds.size()];
				$AudioStreamPlayer2D.play()		
		else:
			sound_cooldown = rand_range(min_sound_cooldown, max_sound_cooldown);
		
		

func update_particle_amount():
	$fire_particles.emitting = fire_amount > 0;
	$smoke_particles.emitting = smoke_amount > 0;

var fire_amount = 1;
var smoke_amount = 1;
var changed_amount = false;
	
func set_fire_and_smoke_amount(fire_amount, smoke_amount):
	if fire_amount != self.fire_amount:
		self.fire_amount = fire_amount;
		self.changed_amount = true;
			
	if smoke_amount != self.smoke_amount:
		self.smoke_amount = smoke_amount;
		self.changed_amount = true;
	update_particle_amount();
	
