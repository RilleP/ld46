extends Label

const interval = 0.5;
var timer = 0;

func _ready():
	pass # Replace with function body.

func _process(delta):
	timer -= delta;
	if timer > 0:
		return;
		
	timer = interval;
	var power = 0.0;
	for fuel in get_tree().get_nodes_in_group("fuel"):
		power += fuel.heat * fuel.remaining_fuel;

	power /= 5.0;
	
	text = "Fire power: %d" % int(power);
