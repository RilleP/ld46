extends StaticBody2D

const min_sapling_spawn_time = 40;
const max_sapling_spawn_time = 70;

const max_spawn_distance = 200;
const min_spawn_distance = 60;

const tree_spawn_min_x = 1400;
const tree_spawn_max_x = 2700;

var next_sapling_spawn_timer = rand_range(min_sapling_spawn_time, max_sapling_spawn_time);

func _ready():
	scale.x = 0.5;
	scale.x = rand_range(0.8, 1.2);
	scale.y = rand_range(0.8, 1.2);
	$Sprite.flip_h = randf() > 0.5;

func chop_down():
	var is_on_fire = $burnable.burning;
	var log_prefab = load("fuel/log.tscn");
	for i in range(0, randi()%3 + 3):
		var log_instance = log_prefab.instance();
		log_instance.rotation_degrees = 90.0;
		log_instance.position = Vector2(self.position.x, self.position.y - i * 150 - 250);
		var impulse_angle = rand_range(0, PI*2.0);
		log_instance.apply_central_impulse(Vector2(cos(impulse_angle), -abs(sin(impulse_angle))) * rand_range(100, 200));
		get_parent().add_child(log_instance);
		if is_on_fire:
			log_instance.add_heat($burnable.heat);
		
	queue_free();

func set_indicated(indicated):
	if indicated:
		$Sprite.material = load("fuel/shine_material.tres");
	else:
		$Sprite.material = null;

func spawn_sapling():
	
	var spawn_pos = position;
	var spawn_distance = rand_range(min_spawn_distance, max_spawn_distance);
	if(randi() % 2 == 0):
		spawn_pos.x += spawn_distance;
	else:
		spawn_pos.x -= spawn_distance;
	if(spawn_pos.x < tree_spawn_min_x || spawn_pos.x > tree_spawn_max_x):
		return;
		
	var all_trees = get_tree().get_nodes_in_group("tree");
	for other in all_trees:
		if abs(other.position.x - spawn_pos.x) < min_spawn_distance:
			return;
	
	var sapling = load("forest/tree.tscn").instance();
	sapling.position = spawn_pos;		
	get_parent().add_child(sapling);

func _process(delta):
	next_sapling_spawn_timer -= delta;
	if next_sapling_spawn_timer <= 0:
		spawn_sapling();
		next_sapling_spawn_timer = rand_range(min_sapling_spawn_time, max_sapling_spawn_time);
