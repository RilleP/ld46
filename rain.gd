extends Node2D

var is_raining = true;

export var max_rain_right = 2000;
export var min_rain_left = -1500;

export var roof_left = 0;
export var roof_right = 500;

export var rain_left = -500;
export var rain_right = 500;
export var rain_ray_margin = 20;

var rain_amount_per_width = 1.0;

var game_time = 0.0;
onready var rain_cooldown = calculate_rain_cooldown(0);
var rain_duration = 0.0;
const rain_base_cooldown = 90;
const rain_base_duration = 25;
const first_rain_drops_delay = 3.0;
var first_rain_drops_delay_timer;

const cooldown_scale_factor = 1000.0;
const duration_scale_factor = 1000.0;

func _ready():
	$particles_left.emitting = false;
	$particles_roof.emitting = false;
	$particles_right.emitting = false;

func calculate_rain_cooldown(game_time):
	var result = rain_base_cooldown * (cooldown_scale_factor / (cooldown_scale_factor + game_time));
	return result;
	
func calculate_rain_duration(game_time):
	var result = rain_base_duration * ((duration_scale_factor + game_time) / duration_scale_factor);
	return result;

func set_particles_min_max(particles, left, right):
	var width = right - left;
	particles.emission_rect_extents.x = width / 2.0;
	particles.position.x = left + width/2.0;
	particles.amount = width;
	particles.emitting = true;
	

func do_rain_check_for_range(left, right, delta):
	var space_state = get_world_2d().direct_space_state;
	var screen_size = get_viewport().size;
	for ray_x in range(left, right, rain_ray_margin):
		var ray_result = space_state.intersect_ray(Vector2(ray_x, -screen_size.y), Vector2(ray_x, 0), [], 1 << 11);
		if(ray_result and ray_result.collider.has_method("add_moist")):
			ray_result.collider.add_moist(delta*0.05);

func _process(delta):
	game_time += delta;
	if !is_raining: 
		get_node("/root/first/UI/RainTimer").text = "Rain will fall in %d:%02d" % [int(rain_cooldown)/60, int(rain_cooldown)%60];
		rain_cooldown -= delta;
		if(rain_cooldown <= 0):
			#var rain_position = rand_range(min_rain_left, max_rain_right);
			#var rain_width = rand_range(min_rain_width, max_rain_width);
			#rain_right = min(max_rain_right, rain_position + rain_width/2.0);
			#rain_left = rain_position - rain_width/2.0;
			rain_right = max_rain_right;
			rain_left = min_rain_left;
			#$particles.emission_rect_extents.x = rain_width;
			#$particles.amount = rain_width * rain_amount_per_width;
			#position.x = (rain_left + rain_right) / 2;
			
			set_particles_min_max($particles_left, min_rain_left, roof_left);
			set_particles_min_max($particles_roof, roof_left, roof_right);
			set_particles_min_max($particles_right, roof_right, max_rain_right);
			
			is_raining = true;
			rain_duration = calculate_rain_duration(game_time);
			first_rain_drops_delay_timer = first_rain_drops_delay;
		return;
	get_node("/root/first/UI/RainTimer").text = "Rain will end in %d:%02d" % [int(rain_duration)/60, int(rain_duration)%60];
	rain_duration -= delta;
	if rain_duration <= 0:
		is_raining = false;
		$particles_left.emitting = false;
		$particles_roof.emitting = false;
		$particles_right.emitting = false;
		rain_cooldown = calculate_rain_cooldown(game_time);
		return;
		
	if rain_left > rain_right:
		return;
	
	if first_rain_drops_delay_timer > 0:
		first_rain_drops_delay_timer-=delta;
	else:
		do_rain_check_for_range(min_rain_left, roof_left, delta);
		do_rain_check_for_range(roof_right, max_rain_right, delta);
			
			
