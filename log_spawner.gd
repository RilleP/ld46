extends Node2D


export var min_spawn_interval = 2.0;
export var max_spawn_interval = 4.0;
onready var spawn_timer = rand_range(min_spawn_interval, max_spawn_interval);

var log_prefab = load("res://fuel/log.tscn");

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	return;
	spawn_timer -= delta;
	if spawn_timer <= 0:
		var spawned_log = log_prefab.instance();
		var screen_size = get_viewport().size;
		spawned_log.position = Vector2(rand_range(screen_size.x*-0.3, screen_size.x*0.3), -screen_size.y)
		get_parent().add_child(spawned_log);
		spawn_timer = rand_range(min_spawn_interval, max_spawn_interval);
