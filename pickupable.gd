extends Node2D

var holder = null;
var hold_offset;

var was_thrown_timer = 0;

onready var initial_mass = get_parent().mass;
onready var initial_layer = get_parent().collision_layer;

signal on_release_from_holder;

func set_indicated(indicated):
	if(indicated):
		get_parent().get_node("Sprite").material = load("fuel/shine_material.tres");
	else:
		get_parent().get_node("Sprite").material = null;
		
func pick_up(by):
	get_parent().collision_layer = 1 << 8;
	holder = by;
	hold_offset = get_parent().position - by.position;
	hold_offset.y = -30;
	hold_offset.x = 20 * by.scale.x;
	get_parent().mass = initial_mass * 0.2;
	#mode = RigidBody2D.MODE_KINEMATIC;

func drop(by):
	if holder == by:
		holder = null;
		was_thrown_timer = 0.5;		
		get_parent().mode = RigidBody2D.MODE_RIGID;
		get_parent().mass = initial_mass;
		get_parent().linear_velocity = by.linear_velocity*1.2;
		
func _physics_process(delta):
	if holder != null:
		var dest_position = holder.position + hold_offset;
		
		var offset = get_parent().position - dest_position;
		get_parent().linear_velocity = -offset/delta/20;

func _process(delta):
	if holder != null:
		hold_offset.x = 20 * holder.look_direction;
	if was_thrown_timer > 0:
		was_thrown_timer -= delta;
		if(was_thrown_timer <= 0):
			get_parent().collision_layer = initial_layer;
