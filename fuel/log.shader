shader_type canvas_item;

uniform float heat = 1.0;
uniform float moist = 0.0;

uniform bool indicated = false;

void fragment() {
	COLOR = texture(TEXTURE, UV);
	vec3 heat_color = vec3(1.0*heat, 0.2*heat, 0.05 *heat);
	COLOR.rgb = mix(COLOR.rgb, heat_color, heat*0.5);
	COLOR.rgb = mix(COLOR.rgb, COLOR.rgb * vec3(1.0 - moist*0.6, 1.0 - moist*0.6, 1.0), moist);
	
	if(indicated) {
		COLOR.rgb = mix(COLOR.rgb, vec3(1, 1, 1), 0.2f);
	
	  	float v = UV.x + TIME*1.5f + UV.y*0.3f;
		v -= floor(v);
		if(v > 0.0f && v < 0.2f) {
			float m;
			if(v > 0.1f) {
				m = (0.2f - v) / 0.2f;
			}
			else {
				m = v / 0.2f;
			}
			COLOR.rgb = mix(COLOR.rgb, vec3(1, 1, 1), m*2.0f);
		}
	}
}