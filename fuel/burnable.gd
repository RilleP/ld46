extends Node2D

export var start_as_burning = false;
var burning = false;
var fire = null;
var smoke = null;

export var start_burning_heat = 1.0;
export var start_smoking_heat = 0.5;
export var heat_add_multiplier = 1.0;
var heat = 0;

var moist = 0;

var gained_heat_timer = 0.0;

export var starting_fuel = 20;
onready var remaining_fuel = starting_fuel;

signal on_burned_up;

func _process(delta):
	if burning:
		remaining_fuel -= delta;
		var lightness = remaining_fuel / starting_fuel;
		get_parent().self_modulate = Color(lightness, lightness, lightness, 1);
		get_parent().scale = Vector2.ONE * lerp(0.8, 1, lightness);
		if(remaining_fuel <= 0):
			emit_signal("on_burned_up");
	elif heat > 0:
		if gained_heat_timer > 0:
			gained_heat_timer -= delta;
		else:
			remove_heat(delta*0.5);
			
	if fire != null:
		fire.heat = heat;

func remove_heat(amount):	
	heat -= amount;
	if heat < 0:
		heat = 0;		
	if heat < start_smoking_heat:
		stop_smoking();
	elif heat < start_burning_heat:
		stop_burning();

func add_heat(amount):
	gained_heat_timer = 1;
	amount *= heat_add_multiplier;
	amount /= (heat*2+1);
	var removed_moist = 0.0;
	if moist > 0:
		moist -= amount;
		if moist < 0:
			removed_moist = amount + moist;
			amount = -moist;
			moist = 0;
		else:
			return amount;
			
	heat += amount;
	if(heat >= start_smoking_heat):
		start_smoking();
	if(heat >= start_burning_heat):
		emit();
	return removed_moist;
		
func add_moist(amount):
	if heat > 0:
		heat -= amount;
		
		if heat < start_smoking_heat:
			stop_smoking();
		elif heat < start_burning_heat:
			stop_burning();
		
		if heat < 0:
			amount = -heat;
			heat = 0;
		else:
			return;
	
	moist = min(moist + amount, 1.0);
	if(moist > 0.5):
		stop_burning();
	

func stop_smoking():
	if fire != null:
		fire.queue_free();
		fire = null;
	burning = false;

func start_smoking():
	if fire == null:
		add_fire();
	
	var fire_amount = 0;
	if burning:
		fire_amount = 1;
	fire.set_fire_and_smoke_amount(fire_amount, 1);

func get_child_of_type(node, type):
	var grand_child_of_type = null;
	for child in node.get_children():
		if child is type:
			return child;
		if grand_child_of_type == null:
			grand_child_of_type = get_child_of_type(child, type);
	return grand_child_of_type;
		

func add_fire():
	assert(fire == null);
	fire = load("fire/fire.tscn").instance();
	add_child(fire);
	var collision_shape = get_child_of_type(get_parent(), CollisionShape2D);
	if(collision_shape != null):
		fire.set_shape_with_collision_shape(collision_shape.shape);
	fire.set_fire_and_smoke_amount(0, 1);

func stop_burning():
	if !burning:
		return;
	burning = false;
	if fire != null:
		fire.set_fire_and_smoke_amount(0, 1);

func emit():
	if burning: 
		return;
	burning = true;
	if fire == null:
		add_fire();
	fire.set_fire_and_smoke_amount(1, 1);
