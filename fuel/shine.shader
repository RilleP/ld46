shader_type canvas_item;

void fragment(){
	COLOR = texture(TEXTURE, UV);
	COLOR.rgb = mix(COLOR.rgb, vec3(1, 1, 1), 0.2f);
	
  	float v = UV.x + TIME*1.5f + UV.y*0.3f;
	v -= floor(v);
	if(v > 0.0f && v < 0.2f) {
		float m;
		if(v > 0.1f) {
			m = (0.2f - v) / 0.2f;
		}
		else {
			m = v / 0.2f;
		}
		COLOR.rgb = mix(COLOR.rgb, vec3(1, 1, 1), m*2.0f);
	}
}