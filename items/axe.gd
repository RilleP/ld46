extends RigidBody2D

func can_do_action(obj):
	if(obj.has_method("chop_down")):
		return true;
	return false;
	
func try_to_action(obj):
	if(obj.has_method("chop_down")):
		chop_tree(obj);
		return true;
	return false;

func chop_tree(tree):
	tree.chop_down();
