extends Control


const fade_out_time = 2.0;
var fade_out = 0;
var is_fading_out = false;

func _process(delta):
	if is_fading_out:
		fade_out += delta;
		modulate.a = 1.0 - (fade_out / fade_out_time);
		if fade_out >= fade_out_time:
			queue_free();
	elif Input.is_action_just_pressed("action") || Input.is_action_just_pressed("jump") || Input.is_action_just_pressed("move_left") || Input.is_action_just_pressed("move_right"):
		is_fading_out = true;
		get_node("/root/first/MusicPlayer").play();
