extends Label

const interval = 0.5;
var timer = 0;

func _process(delta):
	timer -= delta;
	if timer <= 0:
		text = "Score: %d" % get_node("/root/first").score;
		timer = interval;
	
