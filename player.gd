extends RigidBody2D

var score = 0;
var driving = null;

var holding = null; 
var previous_item_to_pick_up = null;

var look_direction = 1;

var playing_action_animation = false;
var chop_sounds = [
	preload("items/chop1.ogg"),
	preload("items/chop2.ogg"),
]

func _ready():
	$AnimatedSprite.connect("animation_finished", self, "on_animation_finished");

func on_animation_finished():
	if playing_action_animation:
		playing_action_animation = false;

func call_method_on_children(node, method_name, args):
	if node.has_method(method_name):
		node.call(method_name, args);
		return true;
	for child in node.get_children():
		if call_method_on_children(child, method_name, args):
			return true;
	return false;

func has_method_on_children(node, method_name):
	if node.has_method(method_name):		
		return true;
	for child in node.get_children():
		if has_method_on_children(child, method_name):
			return true;
	return false;
	
func find_child_with_method(node, method_name):
	if node.has_method(method_name):		
		return node;
	for child in node.get_children():
		var child_with_method = find_child_with_method(child, method_name);
		if child_with_method != null: return child_with_method;
	return null;

func drop_held_object():
	if(holding == null):
		return;
	
	if holding.is_in_group("axe"):
		holding.visible = true;
		$AnimatedSprite.frames = load("player/player_animation_frames.tres");
	
	var child_with_method = find_child_with_method(holding, "drop");
	child_with_method.disconnect("on_release_from_holder", self, "drop_held_object");
	child_with_method.drop(self);
	holding = null;
	
func x_distance_in_look_direction(p):
	return (p.x - self.position.x) * look_direction;

func _process(delta):	
	
	if get_node("/root/first").game_over:
		return;
		
	if driving != null:		
		return;
	
	var item_to_pick_up = null;				
	if holding == null:
		var pickupables = $PickUpArea.get_overlapping_bodies();
		var best_pickupable = null;
		
		for body in pickupables:
			if body.has_method("start_driving"):
				item_to_pick_up = body;				
				best_pickupable = body;
				break;
			else:
				var child_with_method = find_child_with_method(body, "pick_up");
				if child_with_method != null:
					if best_pickupable == null || body.position.y < best_pickupable.position.y-20 || x_distance_in_look_direction(body.position) < x_distance_in_look_direction(best_pickupable.position):
						best_pickupable = body;
		if(best_pickupable != null):
			item_to_pick_up = best_pickupable;
			call_method_on_children(best_pickupable, "set_indicated", true);
		
	else:
		item_to_pick_up = holding;
		if(holding.has_method("can_do_action")):
			var objects_in_area = $PickUpArea.get_overlapping_bodies();
			for body in objects_in_area:
				if holding.can_do_action(body):
					item_to_pick_up = body;					
					break;
		call_method_on_children(item_to_pick_up, "set_indicated", true);
					
			
		
	if(previous_item_to_pick_up != null && previous_item_to_pick_up.get_ref() != null):		
		if(previous_item_to_pick_up.get_ref() != item_to_pick_up):
			call_method_on_children(previous_item_to_pick_up.get_ref(), "set_indicated", false);					
	previous_item_to_pick_up = weakref(item_to_pick_up);
			
	if Input.is_action_just_pressed("action"):
		if holding != null:
			if holding.has_method("try_to_action") && item_to_pick_up != holding:
				holding.try_to_action(item_to_pick_up);
				$AnimatedSprite.play("action");
				playing_action_animation = true;
				if holding.is_in_group("axe"):
					$AudioStreamPlayer2D.stream = chop_sounds[randi() % chop_sounds.size()];
					$AudioStreamPlayer2D.pitch_scale = rand_range(0.8, 1.05);
					$AudioStreamPlayer2D.play();
			else:
				drop_held_object();
		elif item_to_pick_up != null:
			if item_to_pick_up.has_method("start_driving"):
				if item_to_pick_up.start_driving(self):
					call_method_on_children(item_to_pick_up, "set_indicated", false);
					return;
			else:
				var child_with_method = find_child_with_method(item_to_pick_up, "pick_up");
				child_with_method.pick_up(self);
				child_with_method.connect("on_release_from_holder", self, "drop_held_object");
				holding = item_to_pick_up;
				if holding.is_in_group("axe"):
					holding.visible = false;
					$AnimatedSprite.frames = load("player/player_axe_animation_frames.tres");
			
	get_parent().get_node("Camera2D").position.x = position.x;
	
func jump():
	apply_central_impulse(Vector2(0, -150));

func _physics_process(delta):
	
	if driving != null:
		return;
	var grounded = get_colliding_bodies().size() != 0;
	
	var move_direction = Vector2.ZERO;
	if !get_node("/root/first").game_over:
		if Input.is_action_pressed("move_right"):
			move_direction.x += 1;
		if Input.is_action_pressed("move_left"):
			move_direction.x -= 1;
		if grounded and Input.is_action_just_pressed("jump"):
			jump();
		
	if move_direction.length_squared() > 0:
		look_direction = move_direction.x;
		$AnimatedSprite.flip_h = look_direction < 0;
		$PickUpArea.scale.x = look_direction;
		var move_speed = 300;
		if !grounded:
			move_speed *= 0.5;
		apply_central_impulse(move_direction * delta * move_speed);
		
	
		
	if !(playing_action_animation):
		if !grounded:
			$AnimatedSprite.play("falling");
		elif abs(move_direction.x) > 0:
			$AnimatedSprite.play("run");
		else:
			$AnimatedSprite.play("idle");


func _on_PickUpArea_body_entered(body):
	pass
