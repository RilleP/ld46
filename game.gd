extends Node2D

var score = 0;

var game_over = false;

func add_score(amount):
	if !game_over:
		score += amount;

func set_game_over(reason):
	if game_over: return;
	game_over = true;
	$UI/GameOver.visible = true;
	$UI/GameOver/ReasonText.text = reason;
	$UI/GameOver/FinalScoreText.text = "Final Score: %d" % int(score);

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit();
		
	if(game_over): return;
	var power = 0.0;
	for fuel in get_tree().get_nodes_in_group("fuel"):
		power += fuel.heat * fuel.remaining_fuel;
		
	if power <= 0.0:
		set_game_over("Your fire died!");

func house_burned_down():
	set_game_over("Your house burned down!");

func _on_PlayButton_pressed():
	get_tree().reload_current_scene();
